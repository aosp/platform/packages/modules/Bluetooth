// Copyright 2025, The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package {
    default_applicable_licenses: ["Android-Apache-2.0"],
}

rust_defaults {
    name: "bluetooth_offload_leaudio_hci_defaults",
    crate_root: "lib.rs",
    crate_name: "bluetooth_offload_leaudio_hci",
    edition: "2021",
    rustlibs: [
        "android.hardware.bluetooth.offload.leaudio-rust",
        "libbinder_rs",
        "libbluetooth_offload_hci",
        "liblog_rust",
        "liblogger",
    ],
}

rust_library {
    name: "libbluetooth_offload_leaudio_hci",
    defaults: ["bluetooth_offload_leaudio_hci_defaults"],
    vendor_available: true,
    visibility: [
        "//hardware/interfaces/bluetooth:__subpackages__",
        "//packages/modules/Bluetooth/offload:__subpackages__",
        "//vendor:__subpackages__",
    ],
}

rust_test {
    name: "libbluetooth_offload_leaudio_hci_test",
    target: {
        android: {
            test_config_template: ":BluetoothRustTestConfigTemplate",
            test_suites: [
                "general-tests",
                "mts-bt",
            ],
        },
    },
    compile_multilib: "both",
    multilib: {
        lib32: {
            suffix: "32",
        },
        lib64: {
            suffix: "64",
        },
    },
    defaults: ["bluetooth_offload_leaudio_hci_defaults"],
}
